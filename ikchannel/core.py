import discord
from redbot.core import commands, checks, Config
from redbot.core.bot import Red


class IKChannel(commands.Cog):
    """Iron Kingdom RPG Channels"""

    def __init__(self, bot: Red):
        super().__init__()
        self.bot = bot
        self.config = Config.get_conf(self, identifier=13_494_195)
        default_guild = {"category": None, "role": None}
        self.config.register_guild(**default_guild)

    @commands.group(name="ikchannel")
    @checks.guildowner
    @commands.guild_only
    async def _ikchannel(self, ctx: commands.Context):
        """Manage player channels"""
        pass

    @_ikchannel.command(name="mkchannel")
    async def ikchannel_mkchannel(
        self, ctx: commands.Context, user: discord.Member, name: str
    ):

        guild = ctx.guild

        overwrites = {
            guild.default_role: discord.PermissionOverwrite(read_messages=False),
            guild.me: discord.PermissionOverwrite(read_messages=True),
            user: discord.PermissionOverwrite(read_messages=True),
        }

        channel = await guild.create_text_channel(
            f"{name}-court", overwrites=overwrites
        )

    @_ikchannel.command(name="setcategory")
    async def ikchannel_setcategory(
        self, ctx: commands.Context, category: discord.CategoryChannel
    ):
        conf = self.config.guild(ctx.guild)
        await conf.category.set(category.id)
        await ctx.send("Set player channel category to {0}".format(category))

