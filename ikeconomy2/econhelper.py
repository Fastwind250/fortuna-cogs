import discord
from redbot.core import bank

async def transfer(from_: discord.Member, to: discord.Member, amount: int) -> dict:
    info = {
        "sender": {},
        "recipient": {}
    }

    info["sender"]["oldbalance"] = await bank.get_balance(from_)
    info["recipient"]["oldbalance"] = await bank.get_balance(to)

    info["recipient"]["newbalance"] = await bank.transfer_credits(from_, to, amount)
    info["sender"]["newbalance"] = await bank.get_balance(from_)

    return info