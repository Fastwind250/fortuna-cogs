import discord
from redbot.core import bank, checks, commands, errors
from redbot.core.bot import Red  # For hinting purposes

from . import econhelper

class TransferMode:
    def __init__(self, arg):
        if arg == ">":
            self.mode = "from"
        elif arg == "<":
            self.mode = "to"
        else:
            raise RuntimeError

class IKEconomy2(commands.Cog):
    """Iron Kingdom Economy Management V2"""

    def __init__(self, bot: Red):
        super().__init__()
        self.bot = bot

    async def _money_transfer_helper(
        self,
        ctx: commands.Context,
        from_: discord.Member,
        to: discord.Member,
        amount: int
    ):
        try:
            info = await econhelper.transfer(from_, to, amount)
        except (ValueError, errors.BalanceTooHigh) as error:
            msg = str(error)
        else:
            log = {}
            log["Amount transferred"] = "{0:,}".format(amount)
            log["Sender"] = from_
            log["Sender - Previous Balance"] = "{0:,}".format(
                info["sender"]["oldbalance"]
            )
            log["Sender - Current Balance"] = "{0:,}".format(
                info["sender"]["newbalance"]
            )
            log["Recipient"] = to
            log["Recipient - Previous Balance"] = "{0:,}".format(
                info["recipient"]["oldbalance"]
            )
            log["Recipient - Current Balance"] = "{0:,}".format(
                info["recipient"]["newbalance"]
            )
            

            msg = "Transferred {amount:,} from {from_} to {to}".format(amount=amount, to=to, from_=from_)
        await ctx.send(msg)

    @commands.group(name="money")
    async def _money(self, ctx: commands.Context):
        pass

    @_money.command(name="balance")

    @_money.command(name="transfer")
    async def money_transfer(
        self, ctx: commands.Context, to: discord.Member, amount: int
    ):
        self._money_transfer_helper(ctx, ctx.author, to, amount)

    @_money.command(name="otransfer")
    @checks.guildowner()
    async def money_otransfer(
        self,
        ctx: commands.Context,
        from_: discord.Member,
        to: discord.Member,
        amount: int
    ):
        """Owner command: Transfer from one user to another"""
        self._money_transfer_helper(ctx, from_, to, amount)

    @_money.command(name="transfernpc")
    async def money_tnpc(
        self, ctx: commands.Context, npcname: str, mode: TransferMode, user: discord.Member, amount: int
    ):
        """Transfer from NPC to player and vice versa

        Type `from` in the `[mode]` field to transfer from an NPC to a player
        Type `to` in the `[mode]` field to transfer from a player to an NPC
        """
        if not (mode == "<" or mode == ">"):
            await ctx.send_help()
            return
        
        old_balance = bank.get_balance(user)
        if mode.mode == "from":
            new_balance = bank.deposit_credits(user, amount)
        elif mode.mode == "to":
            new_balance = bank.withdraw_credits(user, amount)

        
