import discord
from redbot.core import Config
from enum import Enum


COG_IDENTIFIER = 12_450_505

EMBED_COLOR = 0xEDE291

BALANCE_LIMIT = 2 ** 24

default_guild = {"accounts": {"npc": 1_000_000}, "log_channel": None}

default_member = {"link": None}


class MoneyMode(Enum):
    add = "add"
    remove = "remove"
    set = "set"


class EconomyError(Exception):
    pass


class NoAccount(EconomyError):
    pass


class BalanceTooHigh(EconomyError):
    pass


def _load_config():
    config.register_guild(**default_guild)
    config.register_member(**default_member)


config = Config.get_conf(None, identifier=COG_IDENTIFIER, cog_name="IKEconomy")
_load_config()


async def account_exists(guild: discord.Guild, account: str):
    try:
        await get_balance(guild, account)
    except NoAccount:
        return False
    return True


async def link_exists(guild: discord.Guild, user: discord.Member):
    try:
        acclink = await config.member(user).link()
    except KeyError:
        return False

    if acclink == None:
        return False
    else:
        return True


async def set_balance_compat(guild: discord.Guild, account: str, money: int, moneymode):

    if moneymode == MoneyMode.add:
        return await deposit_money(guild, account, money)
    elif moneymode == MoneyMode.remove:
        return await withdraw_money(guild, account, money)
    elif moneymode == MoneyMode.set:
        return await set_balance(guild, account, money)


async def set_balance(guild: discord.Guild, account: str, money: int) -> int:
    await get_balance(guild, account)
    conf = config.guild(guild)
    excess_balance_msg = "{account}'s balance cannot rise above {money}".format(
        money=BALANCE_LIMIT, account=account
    )
    if money < 0:
        raise ValueError("Negative values not allowed")

    if money >= BALANCE_LIMIT:
        raise BalanceTooHigh(excess_balance_msg)
    await conf.accounts.set_raw(account, value=money)
    return money


async def withdraw_money(guild: discord.Guild, account: str, money: int) -> int:

    balance = await get_balance(guild, account)

    if money < 0:
        raise ValueError("Negative values not allowed")

    if balance < money:
        raise ValueError("Account {0} has insufficient funds".format(account))
    return await set_balance(guild, account, balance - money)


async def deposit_money(guild: discord.Guild, account: str, money: int) -> int:

    balance = await get_balance(guild, account)

    if money < 0:
        raise ValueError("Negative values not allowed")

    return await set_balance(guild, account, balance + money)


async def transfer_money(guild: discord.Guild, from_: str, to: str, money: int) -> dict:
    info = {"sender": {}, "recipient": {}, "amount": 0}

    if money < 0:
        raise ValueError("Negative values not allowed")

    info["sender"]["oldbalance"] = await get_balance(guild, from_)
    info["sender"]["newbalance"] = await withdraw_money(guild, from_, money)

    info["recipient"]["oldbalance"] = await get_balance(guild, to)
    info["recipient"]["newbalance"] = await deposit_money(guild, to, money)

    info["amount"] = money

    return info


async def get_balance(guild: discord.Guild, account: str) -> int:
    try:
        balance = await config.guild(guild).accounts.get_raw(account)
    except KeyError:
        raise NoAccount("Account {0} does not exist".format(account))
    return balance


async def get_log_channel(guild: discord.Guild):
    conf = config.guild(guild)
    channel_id = await conf.log_channel()
    if channel_id:
        channel = guild.get_channel(channel_id)
        return channel
    else:
        return None
