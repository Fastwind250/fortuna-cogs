import discord
from . import econ
from redbot.core.utils import chat_formatting as cf


def make_embed(title=None, **kwargs) -> discord.Embed:
    if title == None:
        title = "*Current information*"
    else:
        title = "*{}*".format(kwargs["title"])
    embed = discord.Embed(color=kwargs["color"], description=title)
    embed.set_author(name="Economy Information")
    embed.add_field(name="Account", value=kwargs["account"], inline=True)
    embed.add_field(
        name="Current Balance", value="{0:,}".format(kwargs["balance"]), inline=True
    )

    return embed


def format_log(log: dict) -> str:
    msg = ""
    for name, desc in log.items():
        msg += "{name} {desc} \n".format(
            name=cf.bold(f"{name}:"), desc=cf.escape(desc, formatting=True)
        )

    return msg


async def send_log(guild: discord.Guild, log: dict, actiontaker: discord.User = None):
    channel = await econ.get_log_channel(guild)
    if channel:
        msg = format_log(log)
        embed = discord.Embed(color=econ.EMBED_COLOR)
        embed.add_field(name="Economy Log", value=msg, inline=False)
        embed.set_author(name="Economy Information")
        embed.set_footer(text="Action taken by {0} (UID: {0.id})".format(actiontaker))
        await channel.send(embed=embed)

