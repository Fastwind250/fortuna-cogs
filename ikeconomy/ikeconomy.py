import discord
from redbot.core import Config, checks, commands
from redbot.core.bot import Red
from redbot.core.utils import chat_formatting

from . import econ, msgutils


class IKEconomy(commands.Cog):
    """Iron Kingdom Economy Management"""

    def __init__(self, bot: Red):
        super().__init__()
        self.bot = bot
        self.embedcolor = econ.EMBED_COLOR

    # Helper Functions

    async def _money_set_helper(self, ctx: commands.Context, account, sum_, moneymode):
        try:
            oldbalance = await econ.get_balance(ctx.guild, account)
            newbalance = await econ.set_balance_compat(
                ctx.guild, account, sum_, moneymode
            )
        except econ.NoAccount as error:
            await ctx.send(error)
        except ValueError as error:
            await ctx.send(error)
        except econ.BalanceTooHigh as error:
            await ctx.send(error)
        else:
            embed = msgutils.make_embed(
                account=account, balance=newbalance, color=self.embedcolor
            )
            embed.add_field(name="Previous Balance", value=oldbalance, inline=True)

            log = {}
            log["Account"] = account
            log["Current Balance"] = "{0:,}".format(newbalance)
            log["Previous Balance"] = "{0:,}".format(oldbalance)

            if moneymode == econ.MoneyMode.add:
                msg = "Money Deposited"
            elif moneymode == econ.MoneyMode.remove:
                msg = "Money Withdrawn"
            else:
                msg = None

            if msg:
                embed.add_field(name=msg, value=sum_, inline=True)
                log[msg] = "{0:,}".format(sum_)

            embed.set_footer(
                text="Action taken by {0} (UID: {0.id})".format(ctx.author)
            )

            await msgutils.send_log(ctx.guild, log, ctx.author)
            await ctx.send(embed=embed)

    # Discord Bot Commands

    @commands.command(name="balance")
    @commands.guild_only()
    async def check_bal(self, ctx: commands.Context):
        """Check own account balance"""

        if not await econ.link_exists(ctx.guild, ctx.author):
            await ctx.send("Account not linked")
            return
        else:
            linkedacc = await econ.config.member(ctx.author).link()

        try:
            bal = await econ.get_balance(ctx.guild, linkedacc)
        except econ.NoAccount:
            await ctx.send("The linked account '{0}' does not exist".format(linkedacc))
            return

        embeddesc = f"Current information for user {ctx.author}"

        embed = msgutils.make_embed(
            title=embeddesc, account=linkedacc, balance=bal, color=self.embedcolor
        )
        await ctx.send(embed=embed)

    @commands.group(name="money")
    @checks.guildowner()
    @commands.guild_only()
    async def _money(self, ctx):
        """Manage balances"""
        pass

    @_money.command(name="link")
    async def money_link(
        self, ctx: commands.Context, user: discord.Member, account: str
    ):
        """Link Discord user to account"""
        await econ.config.member(user).link.set(account)
        await ctx.send("Linked account {0} to Discord user {1}".format(account, user))

    @_money.command(name="unlink")
    async def money_unlink(self, ctx: commands.Context, user: discord.Member):
        """Unlink Discord user from account"""
        if await econ.link_exists(ctx.guild, user):
            await econ.config.member(user).link.clear()
            await ctx.send("Unlinked Discord user {0}".format(user))
        else:
            await ctx.send("Discord user {0} already unlinked".format(user))

    @_money.command(name="balance")
    async def money_balance_other(self, ctx: commands.Context, account: str):
        """Check balance of another account"""
        try:
            balance = await econ.get_balance(ctx.guild, account)
        except econ.NoAccount as error:
            await ctx.send(error)
            return

        embed = msgutils.make_embed(account=account, balance=balance)
        await ctx.send(embed=embed)

    @_money.command(name="register")
    async def money_register(self, ctx: commands.Context, account: str):
        """Register account"""

        if not await econ.account_exists(ctx.guild, account):
            await econ.config.guild(ctx.guild).accounts.set_raw(account, value=0)
            await ctx.send("Registered account {0}".format(account))
        else:
            await ctx.send("Account {0} already registered".format(account))

    @_money.command(name="deposit")
    async def money_deposit(self, ctx: commands.Context, account: str, sum_: int):
        """Deposit money into account"""
        await self._money_set_helper(ctx, account, sum_, econ.MoneyMode.add)

    @_money.command(name="withdraw")
    async def money_withdraw(self, ctx: commands.Context, account: str, sum_: int):
        """Withdraw money from account"""
        await self._money_set_helper(ctx, account, sum_, econ.MoneyMode.remove)

    @_money.command(name="set")
    async def money_set(self, ctx: commands.Context, account: str, sum_: int):
        """Set money amount to account"""
        await self._money_set_helper(ctx, account, sum_, econ.MoneyMode.set)

    @_money.command(name="list")
    async def money_list(self, ctx: commands.Context):
        """List accounts"""
        accounts = await econ.config.guild(ctx.guild).accounts()
        msg = ""
        for account, balance in accounts.items():
            msg += "\n**{0}:** {1:,}".format(account, balance)
        embed = discord.Embed(
            color=self.embedcolor, description=f"**Server:** {ctx.guild}"
        )
        embed.add_field(name="Account List", value=msg, inline=False)
        embed.set_author(name="Economy Information")
        await ctx.send(embed=embed)

    @_money.command(name="links")
    async def money_list_links(self, ctx: commands.Context):
        """List Discord user links to bank accounts"""
        raw_member = await econ.config.all_members(guild=ctx.guild)
        link_list = {}
        for member, data in raw_member.items():
            member_obj = ctx.guild.get_member(member)
            member_str = "{0} (UID: {0.id})".format(member_obj)
            if data["link"]:
                link = data["link"]
            else:
                link = "Not linked"
            link_list[member_str] = link
        msg = msgutils.format_log(link_list)
        embed = discord.Embed(color=econ.EMBED_COLOR)
        if msg:
            emsg = msg
        else:
            emsg = "Empty list"
        embed.add_field(name="Economy Account Links", value=emsg, inline=False)
        embed.set_author(name="Economy Information")
        await ctx.send(embed=embed)

    @_money.command(name="transfer")
    async def money_transfer(
        self, ctx: commands.Context, from_: str, to: str, sum_: int, npcname: str = None
    ):
        """Transfer money from one account to another
        
        To transfer money from an NPC, type *npc* in the `[from_]` field.
        To transfer money to an NPC, do the same in the `[to]` field

        Fill in `[npcname]` if transferring to an NPC.
        The *npc* account is by default set to one million.
        """

        try:
            info = await econ.transfer_money(ctx.guild, from_, to, sum_)
        except econ.NoAccount as error:
            await ctx.send(error)
        except ValueError as error:
            await ctx.send(error)
        except econ.BalanceTooHigh as error:
            await ctx.send(error)
        else:
            log = {}
            log["Amount transferred"] = "{0:,}".format(info["amount"])
            log["Sender"] = from_
            log["Sender - Previous Balance"] = "{0:,}".format(
                info["sender"]["oldbalance"]
            )
            log["Sender - Current Balance"] = "{0:,}".format(
                info["sender"]["newbalance"]
            )
            log["Recipient"] = to
            log["Recipient - Previous Balance"] = "{0:,}".format(
                info["recipient"]["oldbalance"]
            )
            log["Recipient - Current Balance"] = "{0:,}".format(
                info["recipient"]["newbalance"]
            )
            if (to == "npc" or from_ == "npc") and npcname:
                log["NPC"] = npcname
                info["npc"] = npcname
            else:
                info["npc"] = None

            await msgutils.send_log(ctx.guild, log, ctx.author)

            embed = discord.Embed(
                color=self.embedcolor,
                description=msgutils.cf.italics("Account transfer"),
            )
            embed.set_author(name="Economy Information")
            embed.add_field(
                name="Amount transferred",
                value="{0:,}".format(info["amount"]),
                inline=False,
            )
            embed.add_field(name="Sender", value=from_, inline=True)
            embed.add_field(name="Receiver", value=to, inline=True)
            if info["npc"]:
                embed.add_field(name="NPC", value=npcname, inline=True)
            embed.set_footer(
                text="Action taken by {0} (UID: {0.id})".format(ctx.author)
            )

            await ctx.send(embed=embed)

    @commands.group(name="moneylog")
    @checks.guildowner()
    @commands.guild_only()
    async def _moneylog(self, ctx):
        """Set or unset log"""
        pass

    @_moneylog.command(name="set")
    async def moneylog_set(self, ctx: commands.Context, channel: discord.TextChannel):
        """Set channel to log activity"""
        conf = econ.config.guild(ctx.guild)
        if channel.permissions_for(ctx.guild.me).send_messages:
            await conf.log_channel.set(channel.id)
            await ctx.send(
                "Set economy log channel to {channel}".format(channel=channel.mention)
            )
        else:
            await ctx.send(
                "I cannot send messages in {channel}".format(channel=channel.mention)
            )

    @_moneylog.command(name="unset")
    async def moneylog_unset(self, ctx: commands.Context):
        """Unset log channel"""
        conf = econ.config.guild(ctx.guild)
        channel_id = conf.log_channel()
        if channel_id == None:
            await ctx.send("Economy log channel already unset")
        else:
            conf.log_channel.clear()
            await ctx.send("Unset economy log")
